<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Auth routes
 */
Route::group(['namespace' => 'Auth'], function () {

    // Authentication Routes...
    Route::get('/', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('login-form');;
    Route::get('logout', 'LoginController@logout')->name('logout');

    // Registration Routes...

    // if (config('auth.users.registration')) {
        //Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'RegisterController@register');
    // }

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');

    // Confirmation Routes...
    if (config('auth.users.confirm_email')) {
        Route::get('confirm/{user_by_code}', 'ConfirmController@confirm')->name('confirm');
        Route::get('confirm/resend/{user_by_email}', 'ConfirmController@sendEmail')->name('confirm.send');
    }

    // Social Authentication Routes...
    
    // Route::get('social/redirect/{provider}', 'SocialLoginController@redirect')->name('social.redirect');
    // Route::get('social/login/{provider}', 'SocialLoginController@login')->name('social.login');
});

/**
 * Backend routes
 */
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {

    // Dashboard
    Route::get('/', 'DashboardController@index')->name('dashboard');

    //Users
    Route::get('users', 'UserController@index')->name('users');
    Route::get('users/{user}', 'UserController@show')->name('users.show');
    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
    Route::put('users/{user}', 'UserController@update')->name('users.update');
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');
    //Managers
    Route::get('users/manager/create', 'UserController@createManager')->name('users.create');
    Route::post('users/manager/store', 'UserController@storeManager')->name('users.manager.store');
    
    //Stations
    Route::get('stations', 'StationController@index')->name('stations');
    Route::post('stations', 'StationController@store')->name('stations.store');
    Route::get('stations/new', 'StationController@create')->name('stations.create');
    Route::get('stations/{station}', 'StationController@show')->name('stations.show');
    Route::get('stations/{station}/edit', 'StationController@edit')->name('stations.edit');
    Route::put('stations/{station}', 'StationController@update')->name('stations.update');

    //Panels
    Route::get('panels', 'PanelController@index')->name('panels');
    Route::post('panels', 'PanelController@store')->name('panels.store');
    Route::get('panels/new', 'PanelController@create')->name('panels.create');
    Route::get('panels/{panel}', 'PanelController@show')->name('panels.show');
    Route::get('panels/{panel}/edit', 'PanelController@edit')->name('panels.edit');
    Route::put('panels/{panel}', 'PanelController@update')->name('panels.update');

    //Operations
    Route::get('operations', 'OperationController@index')->name('operations');
    Route::post('operations', 'OperationController@index')->name('operations.filter');
    Route::get('operations/bystation/{id}', 'OperationController@filterByStation')->name('operations.filter.bystation');
    Route::post('operations/bystation/{id}', 'OperationController@filterByStation')->name('operations.filter.bystation');
    // Route::get('operations/bystation', 'OperationController@filterByStation')->name('operations.filter.bys');
    Route::get('operations/{operation}', 'OperationController@show')->name('operations.show');
    // Route::post('operations/filter/by-station', 'OperationController@filter')->name('operations.filter');
    
    // Route::post('operations', 'OperationController@store')->name('operations.store');
    // Route::get('operations/new', 'OperationController@create')->name('operations.create');
    // Route::get('operations/{operation}/edit', 'OperationController@edit')->name('operations.edit');
    // Route::put('operations/{operation}', 'OperationController@update')->name('operations.update');
    
    //Permissions
    Route::get('permissions', 'PermissionController@index')->name('permissions');
    Route::get('permissions/{user}/repeat', 'PermissionController@repeat')->name('permissions.repeat');
    Route::get('dashboard/log-chart', 'DashboardController@getLogChartData')->name('dashboard.log.chart');
    Route::get('dashboard/registration-chart', 'DashboardController@getRegistrationChartData')->name('dashboard.registration.chart');
});


// Route::get('/', 'HomeController@index');

/**
 * Membership
 */

// Route::group(['as' => 'protection.'], function () {
//     Route::get('membership', 'MembershipController@index')->name('membership')->middleware('protection:' . config('protection.membership.product_module_number') . ',protection.membership.failed');
//     Route::get('membership/access-denied', 'MembershipController@failed')->name('membership.failed');
//     Route::get('membership/clear-cache/', 'MembershipController@clearValidationCache')->name('membership.clear_validation_cache');
// });
