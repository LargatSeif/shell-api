<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('get-token', 'API\Auth\LoginController@getToken');
Route::post('register', 'API\Auth\RegisterController@create');

//Stations
Route::group(['prefix' => 'stations','middleware' => 'auth:api'], function () {
    Route::get('/','API\StationController@index');
    Route::get('/{id}','API\StationController@findOne');
});

//Panels
Route::group(['prefix' => 'panels','middleware' => 'auth:api'], function () {
    Route::get('/','API\PanelController@index');
    Route::get('/{id}','API\PanelController@findOne');
    Route::get('/bystation/{id}','API\PanelController@findByStationId');
});

//Photos
Route::group(['prefix' => 'photos','middleware' => 'auth:api'], function () {
    Route::get('/','API\PhotoController@index');
    Route::post('/','API\PhotoController@store');
    Route::get('/{id}','API\PhotoController@findOne');
});

//Operation
Route::group(['prefix' => 'operations','middleware' => 'auth:api'], function () {
    Route::get('/','API\OperationController@index');
    Route::post('/','API\OperationController@store');
    Route::get('/{id}','API\OperationController@findOne');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
