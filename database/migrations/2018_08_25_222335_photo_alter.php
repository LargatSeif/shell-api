<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotoAlter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations',function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('agent_id')->unsigned();
            $table->integer('panel_id')->unsigned();
            $table->timestamps();

            $table->foreign('agent_id', 'agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('panel_id', 'panel_id')
                ->references('id')
                ->on('panels')
                ->onDelete('cascade');

        });

         Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('data');
            $table->integer('operation_id')->unsigned();
            $table->timestamps();


            $table->foreign('operation_id')
                ->references('id')
                ->on('operations')
                ->onDelete('cascade');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
