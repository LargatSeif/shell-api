<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOperation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations',function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('agent_id')->unsigned();
            $table->integer('panel_id')->unsigned();
            $table->timestamps();

            $table->foreign('agent_id', 'agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('panel_id', 'panel_id')
                ->references('id')
                ->on('panels')
                ->onDelete('cascade');

        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
