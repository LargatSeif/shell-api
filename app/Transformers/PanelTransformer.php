<?php

namespace App\Transformers;

use App\Panel;
use Flugg\Responder\Transformers\Transformer;

class PanelTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'Operation'
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Panel $panel
     * @return array
     */
    public function transform(Panel $panel)
    {
        return [
            'id' => (int) $panel->id,
            'name' => $panel->name,
            'description'=>$panel->description,
            'station_id'=>$panel->station->id,
            'operations'=>$panel->operations
        ];
    }

    /**
     * Include ReservationDetail
     *
     * @param  Panel $panel
     *
     * @return League\Fractal\Resource\Collection
     */
    public function includeOperations(Panel $panel)
    {
        return $this->collection($panel->operations, new OperationTransformer);
    }
}
