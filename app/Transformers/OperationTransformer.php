<?php

namespace App\Transformers;

use App\Operation;
use Flugg\Responder\Transformers\Transformer;

class OperationTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'Photo'
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Operation $operation
     * @return array
     */
    public function transform(Operation $operation)
    {
        return [
            'id' => (int) $operation->id,
            'agent' => $operation->agent,
            'panel'=>$operation->panel,
            'photos'=>$operation->photos
        ];
    }

     /**
     * Include ReservationDetail
     *
     * @param  Operation $operation
     *
     * @return League\Fractal\Resource\Collection
     */
    public function includePhotos(Operation $operation)
    {
        return $this->collection($operation->photos);
    }
}
