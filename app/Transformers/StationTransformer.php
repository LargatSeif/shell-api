<?php

namespace App\Transformers;

use App\Station;
use Flugg\Responder\Transformers\Transformer;

class StationTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        //'panels'=>PanelTransformer::class,
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [
        //'panels'=>PanelTransformer::class,
    ];

    /**
     * Transform the model.
     *
     * @param  \App\Station $station
     * @return array
     */
    public function transform(Station $station)
    {
        return [
            'id'=>$station->id,
            'description'=>$station->description,
            'address' => $station->address,
            'long' => $station->long,
            'lat' => $station->lat,
            'panels'=> count($station->panels)
        ];
    }

    /**
 * Include related panels.
 *
 * @param  \App\Station $station
 * @return mixed
 */
public function includePanels(Station $station)
{
    return $station->panels;
}
}
