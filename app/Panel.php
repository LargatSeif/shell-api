<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
    public $sortable = ['created_at','station_id'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'panels';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description','station_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the Station that owns the Panel.
     */
    function station(){
        return $this->belongsTo('App\Station');
    }

    function operations(){
        return $this->hasMany('App\Operation');
    }
}
