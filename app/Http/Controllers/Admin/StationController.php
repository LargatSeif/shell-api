<?php

namespace App\Http\Controllers\Admin;

use App\Models\Auth\Role\Role;
use App\Station;
use App\Panel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(auth()->user()->hasRole('admin')){
            return view('admin.stations.index', ['stations' => Station::paginate(10)]);
         } else{
             return redirect()->intended(route('admin.dashboard'));
         }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $station = new Station();
        return view('admin.stations.create',['station' => $station]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Station::create($request->all());
        return redirect()->intended(route('admin.stations'));
    }

    /**
     * Display the specified resource.
     *
     * @param Station $station
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Station $station)
    {
        return view('admin.stations.show', 
            [
                'station' => $station,
                'panels'  => Panel::where('station_id',$station->id)->paginate(10)
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Station $station
     * @return \Illuminate\Http\Response
     */
    public function edit(Station $station)
    {
        return view('admin.stations.edit', ['station' => $station]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Station $station
     * @return mixed
     */
    public function update(Request $request, Station $station)
    {
        $station->address = $request->address;
        $station->description = $request->description;
        $station->long = $request->long;
        $station->lat = $request->lat;
        $station->manager = $request->manager;
        $station->fax = $request->fax;
        $station->fix = $request->fix;
        $station->mobile = $request->mobile;
        $station->save();
        return redirect()->intended(route('admin.stations'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
