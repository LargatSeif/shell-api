<?php

namespace App\Http\Controllers\Admin;

use App\Models\Auth\Role\Role;
use App\Operation;
use App\Station;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class OperationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($operations);
        
        $stations = Station::all();
        if($request->get('init_date') && $request->get('end_date')){
            $init_date = $request->get('init_date');
            $end_date  = $request->get('end_date');
            $operations = Operation::whereBetween('created_at',[$init_date, $end_date])->orderBy('created_at', 'desc')->paginate(10);
            
        }else{
            $operations = Operation::orderBy('created_at', 'desc')->paginate(10);
        }
        

        
        return view('admin.operations.index', 
        [
            'operations' =>  $operations ,
            'stations'  => $stations
        ]);
    }
    public function filter($station,Request $request ){
        if($request->get('init_date') && $request->get('end_date')){
            $init_date = $request->get('init_date');
            $end_date  = $request->get('end_date');
            $operations = Operation::whereBetween('created_at',[$init_date, $end_date])->orderBy('created_at', 'desc')->paginate(10);
            
        }else{
            $operations = Operation::with(['panel'=>function($query){
                $query->where('station_id','=',$station);
            }]);
        }
        
        
    }
    /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
    */
    // public function filter(Request $request)
    // {
    //     // dd($operations);
    //     $stations = Station::find;

    //     $operations = Operation::where('station_id',$request->station_id)->orderBy('created_at', 'desc')->paginate(10);
    //     return view('admin.operations.index', 
    //     [
    //         'operations' =>  $operations ,
    //         'stations'  => $stations
    //     ]);
    // }

    /**
     * Display the specified resource.
     *
     * @param Operation $operation
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Operation $operation)
    {
        return view('admin.operations.show', ['operation' => $operation]);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     $operation = new Operation();
    //     return view('admin.operations.create',['operation' => $operation]);
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     Operation::create($request->all());
    //     return redirect()->intended(route('admin.operations'));
    // }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param Operation $operation
     * @return \Illuminate\Http\Response
     */
    // public function edit(Operation $operation)
    // {
    //     return view('admin.operations.edit', ['operation' => $operation]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Operation $operation
     * @return mixed
     */
    // public function update(Request $request, Operation $operation)
    // {
    //     $operation->address = $request->address;
    //     $operation->description = $request->description;
    //     $operation->long = $request->long;
    //     $operation->lat = $request->lat;
    //     $operation->manager = $request->manager;
    //     $operation->fax = $request->fax;
    //     $operation->fix = $request->fix;
    //     $operation->mobile = $request->mobile;
    //     $operation->save();
    //     return redirect()->intended(route('admin.operations'));
    // }

    
    public function filterByStation($id, Request $request){
        
        $stations = Station::all();
        $ss = Station::find($id);
        if($request->get('init_date') && $request->get('end_date')){
            $init_date = $request->get('init_date');
            $end_date  = $request->get('end_date');
            $operations = Operation::whereBetween('created_at',[$init_date, $end_date])->orderBy('created_at', 'desc')->paginate(10);
        }else{
            $operations = Operation::orderBy('created_at', 'desc')->get();
        }
        
            return view('admin.operations.filter',
            [
                'operations' =>  $operations ,
                'stations'  => $stations,
                'ss'=>$ss,
                'id'=>$id
            ]);
    }
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
