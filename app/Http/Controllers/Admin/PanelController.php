<?php

namespace App\Http\Controllers\Admin;

use App\Models\Auth\Role\Role;
use App\Station;
use App\Panel;
use App\Operation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(auth()->user()->hasRole('admin')){
            return view('admin.panels.index', [
                'panels' => Panel::paginate(10)
                ]
            );
         } else{
             return redirect()->intended(route('admin.dashboard'));
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $panel = new Panel();
        $stations = Station::all();
        return view('admin.panels.create',['panel' => $panel,'stations' => $stations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Panel::create($request->all());
        return redirect()->intended(route('admin.panels'));
    }

    /**
     * Display the specified resource.
     *
     * @param Panel $panel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Panel $panel)
    {
        return view('admin.panels.show', 
            [
                'panel' => $panel,
                'operations'=> Operation::where('panel_id',$panel->id)->paginate(10)
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Panel $panel
     * @return \Illuminate\Http\Response
     */
    public function edit(Panel $panel)
    {

        $stations = Station::all();
        return view('admin.panels.edit', ['panel' => $panel,'stations' => $stations]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Panel $panel
     * @return mixed
     */
    public function update(Request $request, Panel $panel)
    {
        $panel->name = $request->name;
        $panel->description = $request->description;
        $station = Station::find($request->station_id);
        $panel->station_id = $station->id;
        $panel->save();
        return redirect()->intended(route('admin.panels'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
