<?php

namespace App\Http\Controllers\API;

use App\Models\Auth\Role\Role;
use App\Station;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Resources\StationResource;
use Flugg\Responder\Responder;
use App\Transformers\StationTransformer;

class StationController extends Controller
{
    public function index()
    {
      return responder()->success(Station::all(),StationTransformer::class)->respond();
    }

    public function findOne(int $id)
    {
        $station = Station::find($id);
        if($station){
            return responder()->success(Station::find($id),StationTransformer::class)->respond();
        }else{
            return $this->notfound();
        }
      
    }

    function notfound(){
        return response()->json([
                "status"=>200,
                "success"=>false,
                "message"=>"no records matches your request"
            ]);
    }
}
