<?php

namespace App\Http\Controllers\API;

use App\Models\Auth\Role\Role;
use App\Panel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Resources\PanelResource;
use Flugg\Responder\Responder;
use App\Transformers\PanelTransformer;

class PanelController extends Controller
{
    public function index()
    {
        return responder()->success(Panel::all(),new PanelTransformer())->respond();
    }

    
    public function findOne(int $id)
    {
        $panel = Panel::find($id);
        if(count($panel) >0){
            return responder()->success($panel,PanelTransformer::class)->respond();
        }else{
            return $this->notfound();
        }
      
    }
    public function findByStationId(int $id)
    {
        $panels = Panel::where('station_id',$id)->get();
        if(count($panels)>0){
            return responder()->success($panels,PanelTransformer::class)->respond();
        }else{
            return $this->notfound();
        }
      
    }

    protected function notfound(){
        return response()->json([
                "status"=>200,
                "success"=>false,
                "message"=>"no records matches your request"
            ]);
    }

}
