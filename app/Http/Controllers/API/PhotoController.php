<?php

namespace App\Http\Controllers\API;

use App\Models\Auth\Role\Role;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Resources\StationResource;
use App\Http\Resources\PhotoResource;
use Image;
class PhotoController extends Controller
{
    


    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param User $user
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit(User $user)
    // {
    //     return view('admin.users.edit', ['user' => $user, 'roles' => Role::get()]);
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request $request
    //  * @param User $user
    //  * @return mixed
    //  */
    // public function update(Request $request, User $user)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required|max:255',
    //         'email' => 'required|email|max:255',
    //         'active' => 'sometimes|boolean',
    //         'confirmed' => 'sometimes|boolean',
    //     ]);

    //     $validator->sometimes('email', 'unique:users', function ($input) use ($user) {
    //         return strtolower($input->email) != strtolower($user->email);
    //     });

    //     $validator->sometimes('password', 'min:6|confirmed', function ($input) {
    //         return $input->password;
    //     });

    //     if ($validator->fails()) return redirect()->back()->withErrors($validator->errors());

    //     $user->name = $request->get('name');
    //     $user->email = $request->get('email');

    //     if ($request->has('password')) {
    //         $user->password = bcrypt($request->get('password'));
    //     }

    //     $user->active = $request->get('active', 0);
    //     $user->confirmed = $request->get('confirmed', 0);

    //     $user->save();

    //     //roles
    //     if ($request->has('roles')) {
    //         $user->roles()->detach();

    //         if ($request->get('roles')) {
    //             $user->roles()->attach($request->get('roles'));
    //         }
    //     }

    //     return redirect()->intended(route('admin.users'));
    // }

   



    public function index()
    {
      return PhotoResource::collection(Photo::all());
    }

    // public function store(Request $request)
    // {
    //   $station = Station::create([
    //         'description' => $request->description,
    //         'address' => $request->address,
    //         'long' => $request->long,
    //         'lat' => $request->lat,
    //         'manager' => $request->manager,
    //         'mobile' => $request->mobile,
    //         'fax' => $request->fax,
    //         'fix' => $request->fix
    //   ]);

    //   return new StationResource($station);
    // }

    public function findOne(int $id)
    {
        $photo = Photo::find($id);
        if($photo){
            return new PhotoResource($photo);
        }else{
            return response()->json([
                "status"=>200,
                "message"=>"no records matches your request"
            ]);
        }
      
    }

    public function store(Request $request)
    {
        
        $photo = new Photo();
        
        $photo->data = $request->data;
        $photo->type = $request->type;
        $photo->comment = $request->comment;
        $photo->operation_id = $request->operation;
        $photo->save();

        if($photo){
            return response()->json([
                "status"=>200,
                "success"=>true,
                "message"=>"Created succefully"
            ]);
        }else{
            return response()->json([
                "status"=>200,
                "success"=>false,
                "message"=>"Failed to create the record"
            ]);
        }
      
    }

}
