<?php

namespace App\Http\Controllers\API;

use App\Models\Auth\Role\Role;
use App\Operation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Resources\OperationResource;
use App\Transformers\OperationTransformer;
use Flugg\Responder\Responder;

use App\Models\Auth\User\User;
use App\Panel;
class OperationController extends Controller
{
    


    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param User $user
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit(User $user)
    // {
    //     return view('admin.users.edit', ['user' => $user, 'roles' => Role::get()]);
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request $request
    //  * @param User $user
    //  * @return mixed
    //  */
    // public function update(Request $request, User $user)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required|max:255',
    //         'email' => 'required|email|max:255',
    //         'active' => 'sometimes|boolean',
    //         'confirmed' => 'sometimes|boolean',
    //     ]);

    //     $validator->sometimes('email', 'unique:users', function ($input) use ($user) {
    //         return strtolower($input->email) != strtolower($user->email);
    //     });

    //     $validator->sometimes('password', 'min:6|confirmed', function ($input) {
    //         return $input->password;
    //     });

    //     if ($validator->fails()) return redirect()->back()->withErrors($validator->errors());

    //     $user->name = $request->get('name');
    //     $user->email = $request->get('email');

    //     if ($request->has('password')) {
    //         $user->password = bcrypt($request->get('password'));
    //     }

    //     $user->active = $request->get('active', 0);
    //     $user->confirmed = $request->get('confirmed', 0);

    //     $user->save();

    //     //roles
    //     if ($request->has('roles')) {
    //         $user->roles()->detach();

    //         if ($request->get('roles')) {
    //             $user->roles()->attach($request->get('roles'));
    //         }
    //     }

    //     return redirect()->intended(route('admin.users'));
    // }

   



    public function index()
    {
        return responder()->success(Operation::all(), OperationTransformer::class)->respond();
    }

    // public function store(Request $request)
    // {
    //   $station = Station::create([
    //         'description' => $request->description,
    //         'address' => $request->address,
    //         'long' => $request->long,
    //         'lat' => $request->lat,
    //         'manager' => $request->manager,
    //         'mobile' => $request->mobile,
    //         'fax' => $request->fax,
    //         'fix' => $request->fix
    //   ]);

    //   return new StationResource($station);
    // }

    public function findOne(int $id)
    {
        $operation = Operation::find($id);

        $agent = User::find($operation->agent_id);
        $panel = Panel::find($operation->panel_id);

        if($operation){
            return responder()->success($operation, OperationTransformer::class)->respond();
            // return response()->json(
            //     [
            //         "status" => 200,
            //         "success"=>true,
            //         "operation" => $operation
            //     ]);
        }else{
            return response()->json([
                "status"=>200,
                "success"=>false,
                "message"=>"no records matches your request"
            ]);
        }
      
    }

    public function store(Request $request)
    {
        
        $operation = new Operation();
        
        $operation->agent_id = $request->agent_id;
        $operation->panel_id = $request->panel_id;

        $operation->save();

        if($operation){
            return response()->json([
                "status"=>200,
                "success"=>true,
                "message"=>"saved",
                "data"=>$operation
            ]);
        }else{
            return response()->json([
                "status"=>200,
                "success"=>false,
                "message"=>"Failed to create Operation"
            ]);
        }
      
    }

}
