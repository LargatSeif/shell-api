<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id'=>$this->id,
        'description'=>$this->description,
        'address' => $this->address,
        'long' => $this->long,
        'lat' => $this->lat,
        'panels'=> count($this->panels)
      ];
    }
}
