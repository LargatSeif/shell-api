<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OperationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'agent'=>$this->agent,
            'panel'=>$this->panel,
            'photos'=>$this->photos
        ];
    }
}
