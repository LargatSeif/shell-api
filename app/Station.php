<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    public $sortable = ['created_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'address', 'long','lat','manager','mobile','fax','fix'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the Panels for the Station.
     */
    public function panels()
    {
        return $this->hasMany('App\Panel');
    }

}
