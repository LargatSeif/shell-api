<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    public $sortable = ['created_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'operations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['panel_id','agent_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the Station that owns the Panel.
     */
    function agent(){
        return $this->hasOne('App\Models\Auth\User\User','id','agent_id');
    }

    function panel(){
        return $this->hasOne('App\Panel','id','panel_id');
    }

     function stations(){
        return $this->hasManyThrough('App\Station','App\Panel','station_id','id','panel_id','id');
    }

    function photos(){
        return $this->hasMany('App\Photo');
    }

    
    // public function scopeSearch($q)
    // {
    //     return empty(request()->search) ? $q : $q->where('name', 'like', '%'.request()->search.'%');
    // }
}
