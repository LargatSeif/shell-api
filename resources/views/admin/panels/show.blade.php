@extends('admin.layouts.admin')

@section('title', 'Panel', ['name' => $panel->description]))

@section('content')
    <div class="row">
        <table class="table table-striped table-hover">
            <tbody>

            <tr>
                <th>Description</th>
                <td>{{ $panel->name }}</td>
            </tr>
            <tr>
                <th>Address</th>
                <td>{{ $panel->description }}</td>
            </tr>

            <tr>
                <th>Parent Station</th>
                <td>{{ $panel->station->description }} </td>
            </tr>
            <tr>
                <th>Created At</th>
                <td>{{ $panel->created_at }} ({{ $panel->created_at->diffForHumans() }})</td>
            </tr>

            <tr>
                <th>{{ __('views.admin.users.show.table_header_7') }}</th>
                <td>{{ $panel->updated_at }} ({{ $panel->updated_at->diffForHumans() }})</td>
            </tr>
            </tbody>
        </table>
    </div>
    <hr>

    <div class="row">
        @if(count($operations)>0)
        <h3>Children operations :</h3>
        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Agent</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($operations as $operation)
                <tr>
                    <td>{{ $operation->id }}</td>
                    <td>{{ $operation->agent->name }}</td>
                    <td>{{ $operation->created_at }}</td>
                    <td>{{ $operation->updated_at }}</td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="{{ route('admin.operations.show', [$operation->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.show') }}">
                            <i class="fa fa-eye"></i>
                        </a>
                        {{--@if(!$user->hasRole('admin'))--}}
                            {{--<button class="btn btn-xs btn-danger user_destroy"--}}
                                    {{--data-url="{{ route('admin.stations.destroy', [$operation->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.delete') }}">--}}
                                {{--<i class="fa fa-trash"></i>--}}
                            {{--</button>--}}
                        {{--@endif--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-12 text-center">{{ $operations->links() }}</div>
        </div>
        @endif

        @if(count($operations)==0)
        <h3>No operations yet !</h3>
        @endif
    </div>

@endsection