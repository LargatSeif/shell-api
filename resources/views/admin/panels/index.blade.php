@extends('admin.layouts.admin')

@section('title','Panels')

@section('content')
    <div class="row">
        <a href="{{ route('admin.panels.create') }}" class="btn btn-primary pull-right" >Create a new panel </a>
        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Parent Station</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($panels as $panel)
                <tr>
                    <td>{{ $panel->id }}</td>
                    <td>{{ $panel->name }}</td>
                    <td>{{ $panel->description }}</td>
                    <td>{{ $panel->created_at }}</td>
                    <td>{{ $panel->updated_at }}</td>
                    <td>{{ $panel->station->description }}</td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="{{ route('admin.panels.show', [$panel->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.show') }}">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a class="btn btn-xs btn-info" href="{{ route('admin.panels.edit', [$panel->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.edit') }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        {{--@if(!$user->hasRole('admin'))--}}
                            {{--<button class="btn btn-xs btn-danger user_destroy"--}}
                                    {{--data-url="{{ route('admin.stations.destroy', [$station->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.delete') }}">--}}
                                {{--<i class="fa fa-trash"></i>--}}
                            {{--</button>--}}
                        {{--@endif--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-12 text-center">{{ $panels->links() }}</div>
        </div>
        
    </div>
@endsection