@extends('admin.layouts.admin')

@section('title', 'Station', ['name' => $station->description]))

@section('content')
    <div class="row">
        <table class="table table-striped table-hover">
            <tbody>

            <tr>
                <th>Description</th>
                <td>{{ $station->description }}</td>
            </tr>
            <tr>
                <th>Address</th>
                <td>{{ $station->address }}</td>
            </tr>

            <tr>
                <th>Long</th>
                <td>{{ $station->long }}</td>
            </tr>
            <tr>
                <th>Lat</th>
                <td>{{ $station->lat }} </td>
            </tr>
            <tr>
                <th>Panels</th>
                <td>{{ count($station->panels) }} </td>
            </tr>
            <tr>
                <th>Created At</th>
                <td>{{ $station->created_at }} ({{ $station->created_at->diffForHumans() }})</td>
            </tr>

            <tr>
                <th>{{ __('views.admin.users.show.table_header_7') }}</th>
                <td>{{ $station->updated_at }} ({{ $station->updated_at->diffForHumans() }})</td>
            </tr>
            </tbody>
        </table>
        <hr>
    </div>

    <div class="row">
        <h3>Children panels :</h3>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($panels as $panel)
                    <tr>
                        <td>{{ $panel->id }}</td>
                        <td>{{ $panel->name }}</td>
                        <td>{{ $panel->description }}</td>
                        <td>{{ $panel->created_at }}</td>
                        <td>{{ $panel->updated_at }}</td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.panels.show', [$panel->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.show') }}">
                                <i class="fa fa-eye"></i>
                            </a>
                            {{--@if(!$user->hasRole('admin'))--}}
                                {{--<button class="btn btn-xs btn-danger user_destroy"--}}
                                        {{--data-url="{{ route('admin.stations.destroy', [$station->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.delete') }}">--}}
                                    {{--<i class="fa fa-trash"></i>--}}
                                {{--</button>--}}
                            {{--@endif--}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-12 text-center">{{ $panels->links() }}</div>
        </div>
    </div>
@endsection