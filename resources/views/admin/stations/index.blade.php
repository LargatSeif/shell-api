@extends('admin.layouts.admin')

@section('title','Stations')

@section('content')
    <div class="row">
        <a href="{{ route('admin.stations.create') }}" class="btn btn-primary pull-right" >Create a new Station</a>
        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Address</th>
                <th>Description</th>
                <th>Panels</th>
                <th>Created At</th>
                <th>Updated At</th>

                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($stations as $station)
                <tr>
                    <td>{{ $station->id }}</td>
                    <td>{{ $station->address }}</td>
                    <td>{{ $station->description }}</td>
                    <td>{{ count($station->panels) }} <small>panels</small></td>
                    <td>{{ $station->created_at }}</td>
                    <td>{{ $station->updated_at }}</td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="{{ route('admin.stations.show', [$station->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.show') }}">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a class="btn btn-xs btn-info" href="{{ route('admin.stations.edit', [$station->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.edit') }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        {{--@if(!$user->hasRole('admin'))--}}
                            {{--<button class="btn btn-xs btn-danger user_destroy"--}}
                                    {{--data-url="{{ route('admin.stations.destroy', [$station->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.delete') }}">--}}
                                {{--<i class="fa fa-trash"></i>--}}
                            {{--</button>--}}
                        {{--@endif--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        
        <div class="row">
            <div class="col-md-12 text-center">{{ $stations->links() }}</div>
        </div>
    </div>
@endsection