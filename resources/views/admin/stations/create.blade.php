@extends('admin.layouts.admin')

@section('title', 'Create New Station')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            {{ Form::open(['route'=>['admin.stations.store'],'method' => 'post','class'=>'form-horizontal form-label-left']) }}

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address" >
                        Address
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="address" type="text" class="form-control col-md-7 col-xs-12 "
                               name="address" value="{{ $station->address }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address" >
                        Description
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea name="description" id="description" cols="30" rows="10">{{ $station->description }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="log" >
                        Longitude
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="long" type="text" class="form-control col-md-7 col-xs-12 "
                               name="long" value="{{ $station->long }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lat" >
                        Latitude
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="lat" type="text" class="form-control col-md-7 col-xs-12 "
                               name="lat" value="{{ $station->lat }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="manager" >
                        Manager
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="manager" type="text" class="form-control col-md-7 col-xs-12 "
                               name="manager" value="{{ $station->manager }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile" >
                        Mobile
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="mobile" type="tel" class="form-control col-md-7 col-xs-12 "
                               name="mobile" value="{{ $station->mobile }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fix" >
                        Fix
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="fix" type="tel" class="form-control col-md-7 col-xs-12 "
                               name="fix" value="{{ $station->fix }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fax" >
                        Fax
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="fax" type="tel" class="form-control col-md-7 col-xs-12 "
                               name="fax" value="{{ $station->fax }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a class="btn btn-primary" href="{{ URL::previous() }}"> {{ __('views.admin.users.edit.cancel') }}</a>
                        <button type="submit" class="btn btn-success"> {{ __('views.admin.users.edit.save') }}</button>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/users/edit.css')) }}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/users/edit.js')) }}
@endsection