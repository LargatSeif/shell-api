@extends('admin.layouts.admin')

@section('title', 'Operation on panel : '.$operation->panel->name)

@section('content')
    <br>
    <div class="row">
	@foreach($operation->photos as $photo)
        <div class="col-md-3 col-lg-6 text-center" style="border:1px solid black;min-height:400px !important;background-image: url('http://3wga6448744j404mpt11pbx4.wpengine.netdna-cdn.com/wp-content/uploads/2015/05/loading.gif');background-repeat: no-repeat ;background-position:center center;">
            <h2 style="text-transform: uppercase;">{{$photo->type}}</h2>
            <div class="placeholer" style="background-repeat: no-repeat;background-size: contain;">
                <img src="data:image/png;base64,{{$photo->data}}" style="width:300px !important;height:300px !important;" />
            </div>
            <br>         
            <p style="text-transform: capitalize;float:left !important;"><strong>Commentaire</strong>: {{$photo->comment}}</p>
        </div>
	@endforeach
    </div>
    
    
    
@endsection
