@extends('admin.layouts.admin')

@section('title','Operations')

@section('content')
    <div class="row">
       
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Selected station : {{$ss->description}}
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="{{ url( '/admin/operations') }}">All stations</a></li>
            @foreach($stations as $station)
                <li><a href="{{ url( '/admin/operations/bystation',$station->id ) }}">{{$station->description}}</a></li>
            @endforeach
            </ul>
            <div class="col-md-12 text-center">
            {!! Form::open(['url' => '/admin/operations/bystation/'.$id]) !!}

                {!! Form::label('init_date', 'BETWEEN: ') !!}
                {!! Form::input('date', 'init_date', null, ['class' => 'datepicker', 'data-date-format' => 'yy/mm/dd']) !!}

                {!! Form::label('end_date', 'AND: ') !!}
                {!! Form::input('date', 'end_date', null, ['class' => 'datepicker', 'data-date-format' => 'yy/mm/dd']) !!}

                {!! Form::submit('Filtrer') !!}

            {!! Form::close() !!}
            </div>
        </div>
        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Agent</th>
                <th>Panel</th>
                <th>Station</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($operations as $operation)
                @if( $operation->panel->station->id == $id)
                <tr>
                    <td>{{ $operation->id }}</td>
                    <td>{{ $operation->agent->name }}</td>
                    <td>{{ $operation->panel->name }}</td>
                    <td>{{ $operation->panel->station->description}}</td>
                    <td>{{ $operation->created_at }}</td>
                    <td>{{ $operation->updated_at }}</td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="{{ route('admin.operations.show', [$operation->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.show') }}">
                            <i class="fa fa-eye"></i>
                        </a>
                        {{--@if(!$user->hasRole('admin'))--}}
                            {{--<button class="btn btn-xs btn-danger user_destroy"--}}
                                    {{--data-url="{{ route('admin.stations.destroy', [$station->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.delete') }}">--}}
                                {{--<i class="fa fa-trash"></i>--}}
                            {{--</button>--}}
                        {{--@endif--}}
                    </td>
                </tr>
                @endif
            @endforeach
            </tbody>
        </table>
        <div class="row">
        </div>
    </div>
@endsection
